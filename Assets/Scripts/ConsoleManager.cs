﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System;
using System.IO;

public class ConsoleManager : MonoBehaviour {

	public bool ConsoleMode = false;

	private string songminutes;
	private string songseconds;

	private string currentminutes;
	private string currentseconds;

	public List<VisEvent> VisEvents;

	void Start(){


		//ChangeVisColor(Color.cyan);
	}


	void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow)){
		
			ConsoleMode = !ConsoleMode;

			if(ConsoleMode == true){
				GameObject.Find("AudioSource").GetComponent<AudioSource>().Pause();
			}
		}

		if(ConsoleMode){

			GameObject.Find("ConsoleCanvas").GetComponent<Canvas>().enabled = true;
			GameObject.Find("VisualizerCanvas").GetComponent<Canvas>().enabled = false;
			GameObject.Find("Audio Source (1)").GetComponent<AudioSource>().enabled = true;
			//GameObject.Find("AudioSource").GetComponent<AudioSource>().Pause();
			GameObject.Find("Main Camera").GetComponent<Camera>().enabled = false;

		}else{

			GameObject.Find("ConsoleCanvas").GetComponent<Canvas>().enabled = false;
			GameObject.Find("VisualizerCanvas").GetComponent<Canvas>().enabled = true;
			GameObject.Find("Audio Source (1)").GetComponent<AudioSource>().enabled = false;
			GameObject.Find("Main Camera").GetComponent<Camera>().enabled = true;
		}

		Scrollbar Duration = GameObject.Find("Duration").GetComponent<Scrollbar>();

		Duration.value = GameObject.Find("AudioSource").GetComponent<AudioSource>().time / GameObject.Find("AudioSource").GetComponent<AudioSource>().clip.length;

		Text Durtext = GameObject.Find("DurText").GetComponent<Text>();

		Durtext.text = (currentminutes + ":" + currentseconds + " / " + songminutes + ":" + songseconds);
			
		songminutes = Mathf.Floor(GameObject.Find("AudioSource").GetComponent<AudioSource>().clip.length / 60).ToString("00");
		songseconds = Mathf.Floor(GameObject.Find("AudioSource").GetComponent<AudioSource>().clip.length % 60).ToString("00");

		currentminutes = Mathf.Floor(GameObject.Find("AudioSource").GetComponent<AudioSource>().time / 60).ToString("00");
		currentseconds = Mathf.Floor(GameObject.Find("AudioSource").GetComponent<AudioSource>().time % 60).ToString("00");

	}
		

	public void PlaySong(){
		AudioSource source = GameObject.Find("AudioSource").GetComponent<AudioSource>();
		source.Play();

		StartCoroutine("StartAuto");
	}
	public void StopSong(){
		AudioSource source = GameObject.Find("AudioSource").GetComponent<AudioSource>();
		source.Stop();

		StopCoroutine("StartAuto");
	}
	public void PauseSong(){
		AudioSource source = GameObject.Find("AudioSource").GetComponent<AudioSource>();
		source.Pause();

	}

	void PlaySongWOLoop(){
		AudioSource source = GameObject.Find("AudioSource").GetComponent<AudioSource>();
		source.Play();
	}
//	public static ChangeSong(string URL){
//		WWW www = new WWW(URL);
//		yield return www;
//		AudioSource source = GameObject.Find("AudioSource").GetComponent<AudioSource>();
//		source.clip = www.audioClip;
//		source.Play();
//	}
	public void ChangeVisColor(Color color){


		GameObject.FindGameObjectWithTag("SongName").GetComponent<Text>().color = color;
		GameObject.Find("Artist").GetComponent<Text>().color = color;
		GameObject.Find("Trail").GetComponent<TrailRenderer>().material.SetColor("_EmissionColor", color);

		var Bar = GameObject.FindGameObjectsWithTag("BarVis");
		foreach (GameObject BarVis in Bar) BarVis.GetComponent<Renderer>().material.SetColor("_EmissionColor", color);
	
	}


	public void LineAmplitude(float LA){
		GameObject.Find("LineVisualizer").GetComponent<lineVisualizer>().amplitude = LA;
	}
	public void BarAmplitude(float BA){
	
		var Bar = GameObject.FindGameObjectsWithTag("BarVis");
		foreach (GameObject BarVis in Bar) BarVis.GetComponent<barVisualizer>().amplitude = BA;
	}
	public void CreatorName(string name){
	
		GameObject.Find("Artist").GetComponent<Text>().text = name;
	}
	public void SongName (string name){
		GameObject.Find("SongName").GetComponent<Text>().text = name;
	}
//	public static ChangeBackground(string URL){
//		WWW www = new WWW(URL);
//		yield return www;
//		RawImage BG = GameObject.Find("BG").GetComponent<RawImage>();
//		BG.texture = www.texture;
//	}
	public void LoadCustomSong(){

		StartCoroutine("LoadSong");
	}

	IEnumerator LoadSong(){

		AudioSource source = GameObject.Find("AudioSource").GetComponent<AudioSource>();

		WWW www = new WWW("file://" + Application.persistentDataPath + "/VisSong.ogg");
		while( !www.isDone )
			yield return null;
		
		AudioClip clip1 = www.GetAudioClip(false, true);
		source.clip = clip1;


		yield return new WaitForSeconds(0.1f);
	}

	public void LoadAuto(){

		VisEvents.Clear();

		var responseString = " ";

		StreamReader r = File.OpenText(Application.persistentDataPath + "/VisSong.xml"); 
		responseString = r.ReadToEnd(); 
		r.Close(); 
		Debug.Log("File Read"); 

		try
		{
			XDocument appXML = XDocument.Parse(responseString);

			foreach (XElement element in appXML.Descendants("VisEvent"))
			{

				VisEvent.VisEventType EventTypeTemp = VisEvent.VisEventType.SongName;

				if(element.Attribute("type").Value == "LineAmp")
				{
					EventTypeTemp = VisEvent.VisEventType.LineAmp;
				} 
				else if (element.Attribute("type").Value == "BarAmp")
				{
					EventTypeTemp = VisEvent.VisEventType.BarAmp;
				}
				else if (element.Attribute("type").Value == "Pause")
				{
					EventTypeTemp = VisEvent.VisEventType.Pause;
				}
				else if (element.Attribute("type").Value == "Play")
				{
					EventTypeTemp = VisEvent.VisEventType.Play;
				}
				else if (element.Attribute("type").Value == "Stop")
				{
					EventTypeTemp = VisEvent.VisEventType.Stop;
				}
				else if (element.Attribute("type").Value == "SongName")
				{
					EventTypeTemp = VisEvent.VisEventType.SongName;
				}
				else if (element.Attribute("type").Value == "ArtistName")
				{
					EventTypeTemp = VisEvent.VisEventType.ArtistName;
				}
				else if (element.Attribute("type").Value == "VisCol")
				{
					EventTypeTemp = VisEvent.VisEventType.VisualizerColor;
				}

				VisEvents.Add(new VisEvent(element.Value, 0, float.Parse(element.Attribute("time").Value), 1, element.Attribute("data").Value, EventTypeTemp));
			}
		}
		catch (Exception e)
		{
			Console.WriteLine(e.Message);
		}

	}

	IEnumerator StartAuto()
	{
		Debug.Log("Starting Automation");

		var applyobject = GameObject.Find("SceneManager").GetComponent<Apply>();

		foreach (VisEvent EventsToGo in VisEvents){
			yield return new WaitForSeconds(EventsToGo.EventTime);
			if(EventsToGo.eventType == VisEvent.VisEventType.SongName)
				SongName(EventsToGo.EventData);

			if(EventsToGo.eventType == VisEvent.VisEventType.Pause)
				PauseSong();

			if(EventsToGo.eventType == VisEvent.VisEventType.Play)
				PlaySongWOLoop();

			if(EventsToGo.eventType == VisEvent.VisEventType.Stop)
				StopSong();

			if(EventsToGo.eventType == VisEvent.VisEventType.ArtistName)
				CreatorName(EventsToGo.EventData);

			if(EventsToGo.eventType == VisEvent.VisEventType.BarAmp)
				BarAmplitude(float.Parse(EventsToGo.EventData));

			if(EventsToGo.eventType == VisEvent.VisEventType.LineAmp)
				LineAmplitude(float.Parse(EventsToGo.EventData));

			if(EventsToGo.eventType == VisEvent.VisEventType.VisualizerColor)
			{

				var ColorPanel = GameObject.Find("Color").GetComponent<Image>();
				String[] color = EventsToGo.EventData.Split(" "[0]);
				ColorPanel.color = new Color(float.Parse(color[0]), float.Parse(color [1]), float.Parse(color[2]));
				ChangeVisColor(ColorPanel.color);

				GameObject.Find("Color").GetComponent<VisColor>().R.value = float.Parse(color[0]);
				GameObject.Find("Color").GetComponent<VisColor>().G.value = float.Parse(color[1]);
				GameObject.Find("Color").GetComponent<VisColor>().B.value = float.Parse(color[2]);

			}

		} 
			
	}
}
