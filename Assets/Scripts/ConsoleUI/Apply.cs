﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Apply : MonoBehaviour {

	public Text SongName;
	public InputField SongNameText;

	public Text CreatorName;
	public InputField CreatorNameText;

	public Text BA;
	public GameObject[] BarVis;

	public Text LA;
	public lineVisualizer LineVis;

	public InputField BGURL;
	public RawImage ConsoleBG;
	public RawImage VisBG;

	public InputField Song;
	public AudioSource Source;

	// Use this for initialization
	void Start () {

		BarVis = GameObject.FindGameObjectsWithTag("BarVis");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ApplyArtist(){
		CreatorName.text = CreatorNameText.text;
	}

	public void ApplySong(){
		SongName.text = SongNameText.text;
	}

	public void ApplyLA(){
		LineVis.amplitude = float.Parse(LA.text)/100;
	}

	public void ApplyBA(){
		foreach (GameObject bvis in BarVis)
		{
			bvis.GetComponent<barVisualizer>().amplitude = float.Parse(BA.text)/100;
		}
	}

	public void BG(){
	
		StartCoroutine("BGApply");
	}

	public void Audio(){
	
		StartCoroutine("SongApply");
	}

	IEnumerator BGApply(){
	
		WWW www = new WWW(BGURL.text);
		yield return www;
		ConsoleBG.texture = www.texture;
		VisBG.texture = www.texture;
	}

	IEnumerator SongApply(){

		WWW www = new WWW(Song.text);
		yield return www;
		Debug.Log("Finished Downloading Sound Clip");
		Source.clip = www.audioClip;
	}

	public void ApplySongM(string name)
	{
		SongName.text = name;
	}

}
