﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class VisColor : MonoBehaviour {

	public Color color;
	public Image ColorPanel;
	public Slider R;
	public Slider B;
	public Slider G;

	// Use this for initialization
	void Start () {
	
		ColorPanel = GameObject.Find("Color").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {

		GameObject.Find("SceneManager").GetComponent<ConsoleManager>().ChangeVisColor(ColorPanel.color);
	}

	public void UpdateColor(){
	
		color = new Color(R.value, G.value, B.value);
		ColorPanel.color = color;
	}
}
