﻿using UnityEngine;
using System.Collections;

public class SongControl : MonoBehaviour {

	public KeyCode Pause;
	public KeyCode Play;
	public KeyCode Stop;

	private GameObject conman;
	// Use this for initialization
	void Start () {
		conman = GameObject.FindGameObjectWithTag("SM");
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(Pause)){
			conman.GetComponent<ConsoleManager>().PauseSong();
		}
		if(Input.GetKeyDown(Play)){
			conman.GetComponent<ConsoleManager>().PlaySong();
		}
		if(Input.GetKeyDown(Stop)){
			conman.GetComponent<ConsoleManager>().StopSong();
		}
	}
}
