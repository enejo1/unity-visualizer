﻿using UnityEngine;
using System.Collections;


[System.Serializable]

public class VisEvent {

	public string EventName;
	public int EventIndex;
	public float EventTime;
	public int EventSpeed;
	public string EventData;
	public VisEventType eventType;

	public enum VisEventType{

		LineAmp,
		BarAmp,
		BGChange,
		SongChange,
		Pause,
		Play,
		Stop,
		SongName,
		ArtistName,
		VisualizerColor,


	}

	public VisEvent(string name, int index, float time, int speed, string data, VisEventType type){

		EventName = name;
		EventIndex = index;
		EventSpeed = speed;
		EventTime = time;
		EventData = data;
		eventType = type;
	}


	public VisEvent(){


	}
}
