﻿using UnityEngine;
using System.Collections;

public class VisManager : MonoBehaviour {

	public GameObject VisBarObject;
	public GameObject[] VisBars;
	public int AmountOfBars;
	public float Spacing;

	public float width;

	public float VerticalPos;

	// Use this for initialization
	void Awake () {
		for (int i = 0; i < AmountOfBars; i++){
			Vector2 pos = new Vector2 (-8.668f + i * Spacing, VerticalPos);
			Instantiate(VisBarObject, pos, Quaternion.identity);
		}

		VisBars = GameObject.FindGameObjectsWithTag("BarVis");

		for(int i = 0; i < VisBars.Length; i++){
			VisBars[i].GetComponent<Transform>().SetParent(GameObject.Find("Main Camera").GetComponent<Transform>());
			VisBars[i].GetComponent<Transform>().localScale =  new Vector3(width, 1, 1);
		}
	}
	
	// Update is called once per frame
	void Update () {
		float[] spectrum = AudioListener.GetSpectrumData(1024, 0, FFTWindow.BlackmanHarris);

		for(int i = 0; i < AmountOfBars; i++){
			Vector3 prevScale = VisBars[i].transform.localScale;
			prevScale.y = 0.1f + spectrum[i] * VisBars[i].GetComponent<barVisualizer>().amplitude * 4000;
			VisBars[i].transform.localScale = prevScale;

		}
	}
}
