﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class lineVisualizer : MonoBehaviour {

	public int detail = 500;
	public float amplitude = 0.1f;
	private float startPosition;

	// Use this for initialization
	void Start () {
	
		startPosition = transform.localPosition.y;
	}
	
	// Update is called once per frame
	void Update () {

		float[] info = new float[detail];
		AudioListener.GetOutputData(info, 0); 
		float packagedData = 0.0f;

		for(int x = 0; x < info.Length; x++)
		{
			packagedData += System.Math.Abs(info[x]);   
		}
			
		transform.localPosition = new Vector3(transform.localPosition.x, startPosition + packagedData * amplitude, transform.localPosition.z);
	}
}
