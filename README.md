# README #


To use this visualizer, download the source and load in Unity3D v5.4 or above.

An audio visualizer built off of Unity3D (5)

(http://gamedevelopment.tutsplus.com/tutorials/how-to-use-sound-to-make-3d-geometry-in-unity--cms-20456) <-- Based off of this

Please, if you use this for any reason, give credit to me, (preferably through twitter), and give a link to the repo if you want. You're allowed to edit the visualizer scene, (layout, fonts) are the only changeable things not coded in yet, so you're allowed to change them.

If you have any questions, mention me in a tweet or DM me.

Twitter: @gen6games

### What is this repository for? ###

* # Quick summary #
* An audio visualizer built in Unity
* Unity v5.4
* Visualizer v2.0

### How do I get set up? ###

* # Summary of set up #
*Download and open in Unity3D (Don't use the deprecated console scene)
* # Configuration #
*Run in unity editor, or build.
*Up arrow on keyboard to access console in runtime.
*URL (for song and image) doesn't mean HTTP, use ftp://, file://, or etc. if you want
*Song **HAS** to be a .ogg
*Picture **HAS** to be a .png or .jpg/.jpeg
*You can put the file VisSong.ogg in the persistentDataPath (Search Unity Manual), and click the load button in console to workaround doing file:// to load.
* # Dependencies #
Unity game in the editor can't be muted.
* # Deployment instructions #
Build the game, (Leave the game icon as is)

### Notes ###

The console scene is obsolete and will be removed in a future update
Pasting on a mac pastes the text twice, keep that in mind when you copy a url.

### Contribution guidelines ###

* Push to the Community-Pushes branch.

### Who do I talk to? ###

* Repo owner or admin
* @gen6games on Twitter